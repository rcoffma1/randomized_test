describe( "Randomized Test", function() {

	var test = new Test([], [], []);
	var randomizedTest = new Test([], [], []);
	var controlQuestion = "TEST QUESTION";
	var controlChoice = "TEST CHOICE";
	var controlAnswer = "TEST ANSWER";
	var maxQuestions;
	var maxAnswers;
	var debug = false;

	beforeEach(function() {
		test = new Test([], [], []);
		randomizedTest = new Test([], [], []);
		maxQuestions = generateRandomNumber(3,20);
		maxAnswers = generateRandomNumber(3,6);
		buildQuestions(test, maxQuestions, maxAnswers);
		test.questions[0] = controlQuestion;
		test.choices[0][0] = controlChoice;
		test.answers[0][0] = controlAnswer;
		logTest(test);
		randomizedTest = RandomizeTest(test);
	});

	afterEach(function() {
		logTest(randomizedTest, true);
	});

	it('is defined and returns an object', function() {
		expect(typeof window.RandomizeTest(test)).toBe('object');
	});

	it('Contains between 3 and 20 questions', function() {
		expect(3 <= randomizedTest.questions.length <= 20).toBeTruthy();
	});

	it('Contains between 3 and 6 answers per question', function() {
		for(index=0; index < randomizedTest.questions.length; index++) {
			expect(3 <= randomizedTest.answers[index].length <= 6).toBeTruthy();
		}
	});

	it('returns a Test.', function() {
		expect(randomizedTest instanceof Test).toBeTruthy();
	});

	it('returns the original amount of questions, choices, and answers', function() {
		expect(test.questions.length).toEqual(randomizedTest.questions.length);
		expect(test.choices.length).toEqual(randomizedTest.choices.length);
		expect(test.answers.length).toEqual(randomizedTest.answers.length);
	});

	it('returns a question matched with its original choices and answers', function() {
		var questionIndex = randomizedTest.questions.indexOf(controlQuestion);
		expect(randomizedTest.choices[questionIndex].indexOf(controlChoice) > -1).toBeTruthy();
		expect(randomizedTest.answers[questionIndex].indexOf(controlAnswer) > -1).toBeTruthy();
	});

	it('returns a choice matched with its original answer', function() {
		var questionIndex = randomizedTest.questions.indexOf(controlQuestion);
		expect(randomizedTest.choices[questionIndex].indexOf(controlChoice)).toEqual(randomizedTest.answers[questionIndex].indexOf(controlAnswer));
	});

	function generateRandomNumber(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	function buildQuestions(test, maxQuestions, maxAnswers) {
		for (x=0; x<maxQuestions; x++) {
			test.questions.push("question" + x);
			var choices = [];
			var answers = [];
			for (y=0; y<maxAnswers; y++) {
				choices.push("question" + x + " choice" + y);
				answers.push("question" + x + " answer" + y);
			}
			test.choices.push(choices);
			test.answers.push(answers);
		}
	}

	function logTest(test, isRandom) {
		if (debug) {    
			var delimiter = ", ";
			console.log(isRandom? "RANDOMIZED" : "ORIGINAL");
			console.log("QUESTIONS: " + test.questions.join(delimiter));
			for (index in test.questions) {
				console.log("CHOICES: " + test.choices[index].join(delimiter));
				console.log("ANSWERS: " + test.answers[index].join(delimiter));
			}
			console.log("--------------------");
		}
	}

});