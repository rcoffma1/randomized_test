var Test = (Test || function(questions, choices, answers) {
	this.questions = questions;
	this.choices = choices;
	this.answers = answers;
});

function randomizeArray(array) {
	return array.sort(function() {return 0.5 - Math.random()});
}

var RandomizeTest = (function(Test) {

	function Question(question, choices, answers) {
		this.question = question, this.choices = [], this.answers = [];
		var responses = getResponsesFromQuestion(choices, answers);
		randomizeArray(responses);
		for (index in responses) {
			this.choices.push(responses[index].choice);
			this.answers.push(responses[index].answer);
		}
	}

	function Response(choice, answer) {
		this.choice = choice;
		this.answer = answer;
	}

	function getResponsesFromQuestion(choices, answers) {
		var responses = []
		for (index in choices) {
			responses.push(
				new Response(
					choices[index], 
					answers[index]
				)
			);
		}
		return responses;
	}

	function getQuestionsFromTest(test) {
		var questions = [];
		for(index in test.questions) {
			questions.push(
				new Question(
					test.questions[index],
					test.choices[index],
					test.answers[index]
				)
			);
		}
		return questions;
	}

	function getTestFromQuestions(Questions) {
		var questions = [], choices = [], answers =[];
		for (index in Questions) {
			questions.push(Questions[index].question);
			choices.push(Questions[index].choices);
			answers.push(Questions[index].answers);
		}
		return new Test(questions, choices, answers);
	}

	return function(test) {
		var questions = getQuestionsFromTest(test);
		randomizeArray(questions);
		return getTestFromQuestions(questions);
	};

}(Test));